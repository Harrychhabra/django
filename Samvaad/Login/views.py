from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect
from django.urls import reverse
from Login.models import user_login_model

# Create your views here.


@csrf_exempt
def LoginPage(request):
    template = loader.get_template('LoginPage.html')
    error_message = ''
    
    if request.method == 'GET':
        return HttpResponse(template.render())
    else:
        username = request.POST.get('username')
        password = request.POST.get('password')
        
        result = user_login_model.objects.filter(user_name=username,password=password)
        if result.count() != 1:
            error_message = 'Invalid'
        print(result)
        content = {
            'error_message': error_message,
        }
        return HttpResponse(template.render(content))
        # return HttpResponseRedirect('')