from django.db import models

# Create your models here.

class user_login_model(models.Model):
    user_name = models.CharField(max_length = 100)
    password = models.CharField(max_length = 100)
    status = models.CharField(max_length = 20)
    
    def __str__(self):
        return self.user_name + ' | ' + self.status
