from django.urls import path
from . import views

app_name = 'newsfeed'

urlpatterns = [
    path('addCategory/',views.addCategory,name='addCategory'),
    path('addNewsfeed/',views.addNewsfeed,name='addNewsfeed'),
    path('viewNewsfeed/',views.viewNewsfeed,name='viewNewsfeed'),
]
