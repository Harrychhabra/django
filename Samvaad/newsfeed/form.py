from django import forms
from newsfeed.models import Category

class addCategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = "__all__"
        widgets = {
            'category_name' : forms.TextInput(attrs={'style':'color:red'})
        }