from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect
from django.urls import reverse
from newsfeed.models import *
from newsfeed.form import addCategoryForm
import json
import paralleldots
# Create your views here.

paralleldots.set_api_key( "nUV2Ym1gQr8REXKIZQzgKQkDBUkBaUSK6Qukfcx4LwE" )

@csrf_exempt
def addCategory(request):
    addCategory_form = addCategoryForm()
    template = loader.get_template('addCategory.html')
    if request.method == 'GET':
        data = {
            'form':addCategory_form
        }
        return HttpResponse(template.render(data))
    if request.method == 'POST':
        input_category = request.POST.get('category_name')
        input_field = request.POST.get('category_fields')
        print(input_category)
        print(input_field)
        # Category received
        # Search it in tabel
        table_category = Category.objects.filter(category_name = input_category)
        print(table_category)
        # if not present add it to the table
        if table_category.count() == 0:
            addCategory_form = addCategoryForm(request.POST)
            addCategory_form.save()
            return HttpResponse("Table updated")
        # if present
        else:
            # print(table_category)
            # print(table_category[0].category_name)
            data = {
                'category_name':table_category[0].category_name,
                'category_fields':table_category[0].category_fields + input_field,
            }
            # addCategory_form = addCategoryForm(data)
            # addCategory_form.update()
            table_category = Category.objects.get(category_name=input_category)
            table_category.category_fields = table_category.category_fields + input_field
            table_category.save()
            return HttpResponse("Present : " + json.dumps(data))
        return HttpResponse("Post request")


# To add new Newsfeed
@csrf_exempt
def addNewsfeed(request):
    template = loader.get_template('addNewsfeed.html')
    if request.method == 'GET':
        return HttpResponse(template.render())
    else:
        # News feed to be added
        new_newsfeed = NewsFeed()
        new_newsfeed.updated_by = request.POST.get('updated_by')
        new_newsfeed.content = request.POST.get('content')
        new_newsfeed.target_type = ''
        new_newsfeed.category = ''
        categories = Category.objects.all()
        # print(categories[0])
        category = {}
        for cat in categories:
            category[cat.category_name] = json.loads('['+cat.category_fields+']')
            print(cat.category_name + ' ' + cat.category_fields)
        print(category)
        # It does not returns json objects
        # It returns in form = {'field':'value'}
        # but actual json is = { "field" : "value"}
        scores = paralleldots.custom_classifier( request.POST.get('content'), category )
        # score is dict
        print("Received: ")
        print(len(scores['taxonomy']))
        a = {}
        for score in scores['taxonomy']:
            print(score['tag'])
            print(score['confidence_score'])
            a[score['tag']] = score['confidence_score']
        
        # a = {}
        # for s in score['taxonomy']:
            # s = json.dumps(s)
            # a.append(s)
        # print("Final a : ")
        # print(a)
        # Covert score to pure string acceptable by python
        score = json.dumps(score)
        # To extract required info and remove other, covert back it to dict object
        # a = score['taxonomy'][0]
        new_newsfeed.score = a
        # print("News feed: " + new_newsfeed.score)
        # Calculatin score
        new_newsfeed.save();
        return HttpResponse("Added + " + json.dumps(category) + '<br>' + json.dumps(new_newsfeed.score))
        
def viewNewsfeed(request):
    newsfeeds = NewsFeed.objects.all();
    template = loader.get_template('viewNewsfeed.html')
    result = {}
    i = 0
    for news in newsfeeds:
        a = {}
        a['updated_by'] = news.updated_by
        a['content'] = news.content
        # print(news.score)
        # a['score'] = news.score
        # print(a['score'])
        result['newsfeed' + str(i)] = a
        print(news.score['language'])
        result['score' + str(i)] = news.score
        # print(result['newsfeed' + str(i)])
        i = i+1
    print(result)
    return HttpResponse(template.render({'data':result}))
    # score = newsfeeds[0].score
    # score = json.dumps(score)
    # print(json.loads(score))
    # score = json.loads(score)
    # data={
        # 'newsfeed' : score,
    # }
    # print("Data")
    # print(json.loads(data['newsfeed'])['taxonomy'])
    # return HttpResponse(template.render({'name':newsfeeds}))
    data = []
    for news in newsfeeds:
        # a = {}
        # a["updated_by"]  = news.updated_by
        # a["score"] = news.score
        # a["content"] = news.content
        # data.append(a)
        # -----------
        a = []
        a.append(news.updated_by)
        a.append(news.score)
        a.append(news.content)
        data.append(a)
    # data = json.dumps(data)
    # Just for futute refernce
    # Decode it 
    # print(json.loads(json.dumps(json.loads(data[0][1])['taxonomy'][0]))['tag'])
    return HttpResponse(template.render({'names':data}))
    