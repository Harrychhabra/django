from django.db import models
import jsonfield
# Create your models here.

class Category(models.Model):
    category_name = models.CharField(max_length = 100,primary_key = True)
    category_fields = models.CharField(max_length = 1000)
    
    def __str__(self):
        return self.category_name + ' - ' + self.category_fields

class NewsFeed(models.Model):
    updated_by = models.CharField(max_length = 100)
    target_type = models.CharField(max_length = 20)
    content = models.CharField(max_length = 999)
    category = models.CharField(max_length = 20)
    score = jsonfield.JSONField()
    
    def __str__(self):
        return 'Updated by: ' + self.updated_by + ' score: ' + self.score